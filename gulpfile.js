const del = require('del');            //подключение библиотеки  при помощи require
const image = require('gulp-image');
const sass = require('gulp-sass');
const browserSync = require('browser-sync');
const gulp = require('gulp');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const uglify = require('gulp-uglify');
const sourcemaps = require('gulp-sourcemaps');
const rigger = require('gulp-rigger');
const cssmin = require('gulp-minify-css');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');
const reload = browserSync.reload;


gulp.task('clean', function() {                                           // удаляет указаную папку
    return del.sync('build'); // Delete folder before building
});

gulp.task('fonts', function() {                                    //ищет шрифты
    gulp.src('./src/assets/fonts/**/*')
        .pipe(gulp.dest('./build/assets/fonts'))
});

gulp.task('vendors', function() {                            // подключатет библиотеки с vendors
    gulp.src('./src/vendors/**/*')
        .pipe(gulp.dest('./build/vendors/'));
});

gulp.task('image', function() {                                //ищет картинки по определенному пути и сжимает картинки
    gulp.src('./src/assets/img/*')
        .pipe(image())
        .pipe(imagemin({ //Сожмет img
            progressive: true,
            svgoPlugins: [{ removeViewBox: false }],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest('./build/assets/img'))
        .pipe(reload({ stream: true }));                        // перезагрузка браузера

});

gulp.task('html', function() {                               //находит все html, перемещает в билд, перезагружает браузер
    gulp.src('./src/**/*.html')
        .pipe(rigger())
        .pipe(gulp.dest('./build'))
        .pipe(reload({ stream: true }));

});

gulp.task('sass', function() {                                //ищет все файлы scss
    return gulp.src('./src/**/*.scss')
        .pipe(sass().on('error', sass.logError))              // перегоняет scss в css
        .pipe(sourcemaps.init())
        // .pipe(concat('style.css'))             собирает scss  в один фаил css
        .pipe(cssmin())                             //убирает пустые отступы
        .pipe(sourcemaps.write())
        .pipe(autoprefixer())                              //Добавит префиксы для разных браузеров
        .pipe(gulp.dest('./build'))
        .pipe(reload({ stream: true }));

});

gulp.task('js', function() {                             //находит все файлы js
    gulp.src('./src/**/*.js')
        .pipe(rigger())                               //
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./build/'))
        .pipe(reload({ stream: true }));

});

gulp.task('browserSync', function() {                 //
    browserSync({
        server: {
            baseDir: './build/'
        }
    })

});

gulp.task('watch', ['sass', 'html', 'js', 'image', 'browserSync'], function() {
    gulp.watch('./src/styles/**/*.scss', ['sass']);               //
    gulp.watch('./src/assets/img/*', ['image']);
    gulp.watch('./src/**/*.html', ['html']);
    gulp.watch('./src/js**/*.js', ['js']);
    gulp.watch('build/*.html', browserSync.reload);
    gulp.watch("./build/**/*.css").on("change", browserSync.reload);
    gulp.watch('./build/**/*.js').on("change", browserSync.reload);
});

gulp.task('default', ['watch', 'image','vendors', 'fonts']);

